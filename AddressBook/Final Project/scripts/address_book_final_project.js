const form = document.querySelector('form');
const trHead = document.querySelector('#head');
const contactData = document.querySelector('#data');
const nameInput = document.querySelector('#name');
const addressInput = document.querySelector('#add');
const telephoneInput = document.querySelector('#tel');
const emailInput = document.querySelector('#email');
const cityInput = document.querySelector('#city');
const button = document.querySelector('button');

let db

window.onload = () => {
    let request = window.indexedDB.open('contacts', 1)

    request.onerror = () => {
        console.log('Database failed to open')
    }

    request.onsuccess = () => {
        console.log('Database opened successfully')
        db = request.result;
        displayData();
    }
    request.onupgradeneeded = (e) => {
        let db = e.target.result;
        let objectStore = db.createObjectStore('contacts', { keyPath: 'id', autoIncrement:true });
    
        objectStore.createIndex('name', 'name', { unique: false });
        objectStore.createIndex('address', 'address', { unique: false });
        objectStore.createIndex('telephone', 'telephone', { unique: false });
        objectStore.createIndex('email', 'email', { unique: false });
        objectStore.createIndex('city', 'city', { unique: false });
    
        console.log('Database setup complete');
      };

    form.onsubmit = (e) => {
        e.preventDefault()
        let newItem = { name: nameInput.value, address: addressInput.value, telephone: telephoneInput.value, email: emailInput.value, city: cityInput.value };
        let transaction = db.transaction(['contacts'], 'readwrite');
        let objectStore = transaction.objectStore('contacts');
        var request = objectStore.add(newItem)

        request.onsuccess = () => {
            nameInput.value = '';
            addressInput.value = '';
            telephoneInput.value = '';
            emailInput.value = '';
            cityInput.value = '';
        };

        transaction.oncomplete = () => {
            console.log('Transaction completed: database modification finished.');
            displayData();
        };

        transaction.onerror = () => {
            console.log('Transaction not opened due to error');
        };
    }

    function displayData() {
        while (contactData.firstChild) {
          contactData.removeChild(contactData.firstChild);
        }

        let objectStore = db.transaction('contacts').objectStore('contacts');

        objectStore.openCursor().onsuccess = (e) => {
          let cursor = e.target.result;

          if(cursor) {
            let tr = document.createElement('tr');
            let tdName = document.createElement('td'); 
            let tdAddress = document.createElement('td');
            let tdTelephone = document.createElement('td');
            let tdEmail = document.createElement('td');
            let tdCity = document.createElement('td');

            tr.appendChild(tdName);
            tr.appendChild(tdAddress);
            tr.appendChild(tdTelephone);
            tr.appendChild(tdEmail);
            tr.appendChild(tdCity);
            contactData.appendChild(tr);
    
            tdName.textContent = cursor.value.name
            tdAddress.textContent = cursor.value.address
            tdTelephone.textContent = cursor.value.telephone
            tdEmail.textContent = cursor.value.email
            tdCity.textContent = cursor.value.city
    
            tr.setAttribute('data-contact-id', cursor.value.id);
    
            let deleteBtn = document.createElement('button');
            tr.appendChild(deleteBtn);
            deleteBtn.textContent = 'Delete';
    
            deleteBtn.onclick = deleteItem;

            cursor.continue();
          } 
          else {
            if(!contactData.firstChild) {
              let para = document.createElement('p');
              para.textContent = 'No contact stored.'
              contactData.appendChild(para);
            }
            console.log('Notes all displayed');
          }
        };
      }

      function deleteItem(e) {
        let contactId = Number(e.target.parentNode.getAttribute('data-contact-id'));
        let transaction = db.transaction(['contacts'], 'readwrite');
        let objectStore = transaction.objectStore('contacts');
        let request = objectStore.delete(contactId);
    
        transaction.oncomplete = () => {
          e.target.parentNode.parentNode.removeChild(e.target.parentNode);
          console.log('Contact ' + contactId + ' deleted.');

          if(!contactData.firstChild) {
            let para = document.createElement('p');
            para.textContent = 'No contacts stored.';
            contactData.appendChild(para);
          }
        };
      }
      setTimeout( () => {
        const notice = document.getElementById('coronavirus-notice');
        notice.remove();
        console.log('removed')
        }, 5000
    )
}